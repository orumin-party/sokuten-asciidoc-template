UNAME = $(shell uname)

ifeq ($(UNAME),Darwin)
	VIEWER=open
else
	VIEWER=xdg-open
endif

BINS=.bundle/bin
asciidoctor-pdf=$(BINS)/asciidoctor-pdf
asciidoctor-latex=$(BINS)/asciidoctor-latex
asciidoctor=$(asciidoctor-pdf) $(asciidoctor-latex)

SRC=manuscript.adoc
PDF=$(SRC:%.adoc=%.pdf)
TEX=$(SRC:%.adoc=%.tex)
INTER=intermediate.adoc

.PHONY: all clean semi-clean

.SECONDARY: $(INTER)

define escaping
	sed -e 's|@|\&#64;|g' $< > $(INTER)
endef

define postprocess
	sed -i -e 's|"`|“|g' -e 's|`"|”|g' $@
	sed -i -e 's|、|，|g' $@
	sed -i -e 's|#|\\#|g' $@
	sed -i -e 's|\\href{\([^}]*\)}{\([^}]*\)}|\\url{\2}|g' $@
endef

all: $(asciidoctor) $(PDF) 

convert: $(TEX) semi-clean

$(asciidoctor):
	bundle config github.https true
	bundle --path=.bundle --binstubs=$(BINS)

%.pdf: %.adoc
	$(asciidoctor-pdf) -r asciidoctor-pdf-cjk -a pdf-style=default-with-fallback-font $<

$(PDF): $(SRC)

$(TEX): $(INTER)
	$(asciidoctor-latex) -a header=no $< -o $(TEX)
	$(postprocess)

$(INTER): $(SRC)
	$(escaping)

clean:
	@rm -f $(PDF) $(TEX)

semi-clean:
	@rm -f $(INTER) newEnvironments.tex
